import fetch from 'node-fetch';

export const post = async (event) => {
  try {
    const body = JSON.parse(event.body);
    const {
      action,
      description,
      state,
      time_estimate,
      total_time_spent,
      updated_at,
      created_at,
      title,
      url,
      iid,
    } = body.object_attributes;
    const { assignees } = body;

    const embed = {
      title: `Issue ${iid}: ${title} ${action}`,
      description,
      url,
      fields: [{
        name: 'Created On',
        value: created_at,
      }, {
        name: 'Updated On',
        value: updated_at,
      }, {
        name: 'State',
        value: state,
      }],
    };

    if (assignees && assignees.length > 0) {
      embed.fields.push({
        name: 'Assigned To',
        value: assignees[0].username,
      });
    }

    if (time_estimate) {
      embed.fields.push({
        name: 'ETA',
        value: `${(time_estimate / 3600).toFixed(2)} Hour(s)`,
      });
    }
    if (total_time_spent) {
      embed.fields.push({
        name: 'Time spent',
        value: `${(total_time_spent / 3600).toFixed(2)} Hour(s)`,
      });
    }

    const postResult = await fetch(
      'https://discordapp.com/api/webhooks/489203188881162240/eFxTjXy2TonUjFwX9XpY3Of5-qpHcTIb-PdTaHkNeMculNJzOHoxYEy_pKa4ztZaVvFZ',
      {
        method: 'POST',
        body: JSON.stringify({
          embeds: [
            embed,
          ],
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    return {
      statusCode: postResult.status,
      body: JSON.stringify({
        message: postResult.statusText,
      }),
    };
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: e.message,
      }),
    };
  }
};
