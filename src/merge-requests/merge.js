import fetch from 'node-fetch';

export const post = async (event) => {
  try {
    const body = JSON.parse(event.body);
    const {
      target_branch,
      source_branch,
      description,
      state,
      action,
      id,
      title,
      url,
      work_in_progress,
    } = body.object_attributes;

    const embed = {
      title: `Merge Request ${id}: ${title} ${action}`,
      description,
      url,
      fields: [{
        name: 'Source',
        value: source_branch,
      }, {
        name: 'Target',
        value: target_branch,
      }, {
        name: 'State',
        value: state,
      }, {
        name: 'Completed',
        value: work_in_progress ? 'No' : 'Yes',
      }],
    };

    const postResult = await fetch(
      'https://discordapp.com/api/webhooks/489933843168624640/EqKY7tyZCwclTWphPutB3RRyZkLDDuj_y1dtifBgj0C74gR-G9QJG6McILYJ3_W8ykJx',
      {
        method: 'POST',
        body: JSON.stringify({
          embeds: [
            embed,
          ],
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    return {
      statusCode: postResult.status,
      body: JSON.stringify({
        message: postResult.statusText,
      }),
    };
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: e.message,
      }),
    };
  }
};
