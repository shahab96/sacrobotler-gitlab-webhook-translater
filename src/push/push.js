import fetch from 'node-fetch';

export const post = async (event) => {
  try {
    const body = JSON.parse(event.body);
    const {
      user_username,
      ref,
      commits,
    } = body;

    const embed = {
      title: `${user_username} pushed ${commits.length}+ commits to ${ref}`,
      fields: [],
    };

    commits.forEach(commit => embed.fields.push({
      name: commit.message,
      value: commit.url,
    }));

    const postResult = await fetch(
      'https://discordapp.com/api/webhooks/489933843168624640/EqKY7tyZCwclTWphPutB3RRyZkLDDuj_y1dtifBgj0C74gR-G9QJG6McILYJ3_W8ykJx',
      {
        method: 'POST',
        body: JSON.stringify({
          embeds: [
            embed,
          ],
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    return {
      statusCode: postResult.status,
      body: JSON.stringify({
        message: postResult.statusText,
      }),
    };
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: e.message,
      }),
    };
  }
};
